package ServiceInterface;

import java.util.List;

import Model.Customer;

public interface CustomerServiceInterface {

	public List<Customer> getall();

	public Customer getCuByNic(String nic);

	public boolean addCustomer(Customer customer);

	public Customer getCusById(int id);

	public boolean DeleteCusById(int id);
}
