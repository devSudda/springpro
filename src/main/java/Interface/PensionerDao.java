package Interface;

import Model.Pension;
import Model.Pensioner;
import Model.PensionerList;

public interface PensionerDao {

	public Pensioner getPensioner(String nic);
	public PensionerList getAllpensioner();
	public Pension getpension(Pension pension);
	
	
}
