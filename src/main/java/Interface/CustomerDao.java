package Interface;

import java.util.List;

import Model.Customer;

public interface CustomerDao {

	public Customer getAllcus();

	public List<Customer> getAll();

	public Customer getCusByNic(String nic);

	public boolean addCustomer(Customer customer);

	public Customer getCusById(int id);

	public boolean DeleteCusById(int id);
}
