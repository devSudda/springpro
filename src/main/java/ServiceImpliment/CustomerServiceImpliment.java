package ServiceImpliment;

import java.util.List;

import DaoImpliment.CustomerImpliment;
import Model.Customer;
import ServiceInterface.CustomerServiceInterface;


public class CustomerServiceImpliment implements CustomerServiceInterface {

	@Override
	public List<Customer> getall() {

		return new CustomerImpliment().getAll();
	}

	@Override
	public Customer getCuByNic(String nic) {

		return new CustomerImpliment().getCusByNic(nic);
	}

	@Override
	public boolean addCustomer(Customer customer) {

		return new CustomerImpliment().addCustomer(customer);
	}

	@Override
	public Customer getCusById(int id) {
//		Customer cus = new Customer();
//		CustomerServiceImpliment ser =  new CustomerServiceImpliment();
//		cus= ser.getCusById(id);
//		
		
		return new CustomerImpliment().getCusById(id);
	}

	@Override
	public boolean DeleteCusById(int id) {
		
		return new CustomerImpliment().DeleteCusById(id);
	}

}
