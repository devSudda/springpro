package SpringProject.springpro;

import java.util.List;

import javax.websocket.server.PathParam;

import org.apache.catalina.connector.Response;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import Model.Customer;
import ServiceImpliment.CustomerServiceImpliment;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/customer")
public class CustomerController {

	@GetMapping("/all")
	public List<Customer> get() {
		return new CustomerServiceImpliment().getall();
	}

	@GetMapping("/{nic}")
	public Customer getCusByNic(@PathVariable String nic) {
		return new CustomerServiceImpliment().getCuByNic(nic);
	}

	@GetMapping("/id/{id}")
	public Customer getCusById(@PathVariable int id) {
		return new CustomerServiceImpliment().getCusById(id);
	}

	@PostMapping("add")
	public boolean addCustomer(@RequestBody Customer customer) {
		return new CustomerServiceImpliment().addCustomer(customer);

	}

	@DeleteMapping("/delete/{id}")
	public boolean DeleteCusById(@PathVariable int id) {

		return new CustomerServiceImpliment().DeleteCusById(id);

	}

}
