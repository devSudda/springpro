package SpringProject.springpro;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;

@SpringBootApplication
public class SpringproApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringproApplication.class, args);
		System.out.println("system on");
	}
	protected SpringApplicationBuilder configure (SpringApplicationBuilder builder) {
		return builder.sources(SpringproApplication.class);
	}
	

}
