package DaoImpliment;

import java.beans.Transient;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import Dao.BaseDao;
import Interface.CustomerDao;
import Model.Customer;

public class CustomerImpliment extends BaseDao implements CustomerDao {

	boolean flag = false;

	public Connection con = getConnection();

	@Override
	public Customer getAllcus() {
		Customer cus;

		Connection con;

		String sql = "";

		return null;
	}

	@Override
	public List<Customer> getAll() {

		List<Customer> cusList = new ArrayList<Customer>();

		String sql = "SELECT name,phone, nic,email,id FROM `Customer` ";

		try {
			PreparedStatement ps = con.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				Customer cus = new Customer();
				cus.setName(rs.getString(1));
				cus.setPhone(rs.getString(2));
				cus.setNic(rs.getString(3));
				cus.setEmail(rs.getString(4));
				cus.setId(rs.getInt(5));
				cusList.add(cus);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return cusList;
	}

	@Override
	public Customer getCusByNic(String nic) {
		Customer cus = new Customer();
		String sql = "SELECT name,phone, nic,email,id FROM `Customer` WHERE nic=?";
		try {
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setString(1, nic);
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				cus.setName(rs.getString(1));
				cus.setPhone(rs.getString(2));
				cus.setNic(rs.getString(3));
				cus.setEmail(rs.getString(4));
				cus.setId(rs.getInt(5));
			}
		} catch (Exception e) {
			e.printStackTrace();
//			throw new 
		}
		return cus;
	}

	@Override
	public boolean addCustomer(Customer customer) {

		String sql = "INSERT INTO `Customer`(`name`, `phone`, `nic`, `email`) VALUES (?,?,?,?)";

		try {
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setString(1, customer.getName());
			ps.setString(2, customer.getPhone());
			ps.setString(3, customer.getNic());
			ps.setString(4, customer.getEmail());

			return (ps.executeUpdate() != 0 ? !flag : flag);

		} catch (Exception e) {
			e.printStackTrace();
			return flag;
		}

	}

	@Override
	public Customer getCusById(int id) {
		String sql = "SELECT name,phone,nic,email,ID FROM `Customer` WHERE id = ? AND ACTIVESTATUS = 1 ";
		Customer cus = new Customer();
		try {
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setInt(1, id);
			System.out.println(ps);
			try {
				ResultSet rs = ps.executeQuery();
				if (rs.next()) {
					cus.setName(rs.getString(1));
					cus.setPhone(rs.getString(2));
					cus.setNic(rs.getString(3));
					cus.setEmail(rs.getString(4));
					cus.setId(id);
				}
				else {
					return cus;
				}
			} catch (Exception e) {
				e.printStackTrace();
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return cus;
	}

	@Override
	public boolean DeleteCusById(int id) {
		String sql = "UPDATE Customer set ACTIVESTATUS = 0 WHERE id = ? ";
		boolean bool = true;
		try {
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setInt(1, id);
			
		return	bool = (ps.execute() != true ? true : false);

			
		} catch (Exception e) {
			e.printStackTrace();
//			return bool;
		}
		return bool;

	}

}
