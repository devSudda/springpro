package DaoImpliment;

import Interface.CustomerDao;
import Interface.PensionerDao;
import Interface.StaffDao;
import Model.Staff;


public class DaoManager {

	public static PensionerDao pensionerdao() {
		return new PensionerImpliment();
		
	}
	public static StaffDao staffdao() {
		return new StaffImpliment();
	}
	
	public static CustomerDao customerDao() {
		return new CustomerImpliment();
	}
}
