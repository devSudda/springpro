package Dao;

import java.sql.Connection;

import org.slf4j.Logger;

import DataConnec.DbConnectionManager;

public class BaseDao {

	
	public Connection getConnection() {
//		Logger.info("Conection Execute");
		return DbConnectionManager.MANAGER.getConnection();
	}
	
}
