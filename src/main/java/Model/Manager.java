package Model;

import java.util.List;

public class Manager extends Staff {

	private String mobile;
	private Loan loan;
	

	

	public Loan getLoan() {
		return loan;
	}

	public void setLoan(Loan loan) {
		this.loan = loan;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

}
