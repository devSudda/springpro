package Model;

import java.util.List;

public class Customer {

	private String name;
	private String nic;
	private Loan loan;
	
	private String phone;
	private String email;
	private int id;
	private String ACTIVESTATUS;
	
	
	
	public String getACTIVESTATUS() {
		return ACTIVESTATUS;
	}
	public void setACTIVESTATUS(String aCTIVESTATUS) {
		ACTIVESTATUS = aCTIVESTATUS;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}

	
	
	
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getNic() {
		return nic;
	}
	public void setNic(String nic) {
		this.nic = nic;
	}
	public Loan getLoan() {
		return loan;
	}
	public void setLoan(Loan loan) {
		this.loan = loan;
	}
	
	
	
	
}
