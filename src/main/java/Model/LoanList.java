package Model;

import java.util.List;

public class LoanList {

	private List<Loan> loan;

	public List<Loan> getLoan() {
		return loan;
	}

	public void setLoan(List<Loan> loan) {
		this.loan = loan;
	}
	
}
