package DataConnec;

import java.sql.Connection;
import java.sql.DriverManager;

import DbConnection.ApiConsts;

public enum DbConnectionManager {
MANAGER;
	
	public Connection getConnection() {
		Connection conn = null;
		try {
			Class.forName(ApiConsts.JDBC_DRIVER);
			conn = DriverManager.getConnection(ApiConsts.DB_URL, ApiConsts.USER, ApiConsts.PASS);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return conn;
	}
}
